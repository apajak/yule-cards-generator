from PIL import Image, ImageDraw, ImageFont
# from tqdm import tqdm
# Global variables
card_size = (640, 960)
top_left_icon_position = (30, 100)
top_left_value_position = (45, 20)
bottom_right_icon_position = (card_size[0] - 95, card_size[1] - 180)
bottom_right_value_position = (card_size[0] - 80, card_size[1] - 180)
small_icon_size = (int(card_size[0] * 0.1), int(card_size[1] * 0.07))
as_icon_size = (int(card_size[0] * 0.5), int(card_size[1] * 0.4))
big_icon_size = (int(card_size[0] * 0.2), int(card_size[1] * 0.14))
medium_icon_size = (int(card_size[0] * 0.15), int(card_size[1] * 0.1))
value_icon_size = (int(card_size[0] * 0.1), int(card_size[1] * 0.07))
figure_size = (int(card_size[0] * 0.7), int(card_size[1] * 0.8))

def resize_icon(icon, target_size):
    icon = icon.convert('RGBA')
    ratio = min(target_size[0] / icon.width, target_size[1] / icon.height)
    new_size = (int(icon.width * ratio), int(icon.height * ratio))
    return icon.resize(new_size, Image.LANCZOS)

def resize_image(image, target_size):
    """Resize the image while maintaining aspect ratio."""
    original_width, original_height = image.size
    ratio = min(target_size[0] / original_width, target_size[1] / original_height)
    new_size = (int(original_width * ratio), int(original_height * ratio))
    return image.resize(new_size, Image.LANCZOS)

def place_icons(card, icon, value, card_size, icon_sizes):
    # Détermination de la taille de l'icône basée sur la valeur de la carte
    if value == 'A':
        icon_size = icon_sizes['as']
    elif value in ['2', '3']:
        icon_size = icon_sizes['big']
    else:
        icon_size = icon_sizes['medium']

    # Redimensionnement de l'icône
    resized_icon = resize_icon(icon, icon_size)

    # Définition des positions des colonnes et des rangées
    col_positions = {
        'left': int(card_size[0] * 0.27),
        'center': int(card_size[0] * 0.5),
        'right': int(card_size[0] * 0.73)
    }
    row_positions = {
        'top': int(card_size[1] * 0.15),
        'upper_center': int(card_size[1] * 0.25),
        'upper_mid': int(card_size[1] * 0.35),
        'center': int(card_size[1] * 0.5),
        'lower_mid': int(card_size[1] * 0.65),
        'lower_center': int(card_size[1] * 0.75),
        'bottom': int(card_size[1] * 0.85)
    }

    # Initialisation de la liste des positions
    positions = calculate_positions(value, col_positions, row_positions)

    # Placement des icônes sur la carte
    for pos in positions:
        icon_centered_x = pos[0] - resized_icon.width // 2
        icon_centered_y = pos[1] - resized_icon.height // 2
        card.paste(resized_icon, (icon_centered_x, icon_centered_y), resized_icon)

def calculate_positions(value, col_positions, row_positions):
    positions = []
    # Define icon positions based on the card value
    if value == 'A':
        positions.append((col_positions['center'], row_positions['center']))
    elif value == '2':
        positions.extend([
            (col_positions['center'], row_positions['top']),
            (col_positions['center'], row_positions['bottom'])
        ])
    elif value == '3':
        positions.extend([
            (col_positions['center'], row_positions['top']),
            (col_positions['center'], row_positions['center']),
            (col_positions['center'], row_positions['bottom'])
        ])
    elif value == '4':
        positions.extend([
            (col_positions['left'], row_positions['top']),
            (col_positions['right'], row_positions['top']),
            (col_positions['left'], row_positions['bottom']),
            (col_positions['right'], row_positions['bottom'])
        ])
    elif value == '5':
        positions.extend([
            (col_positions['left'], row_positions['top']),
            (col_positions['right'], row_positions['top']),
            (col_positions['left'], row_positions['bottom']),
            (col_positions['right'], row_positions['bottom']),
            (col_positions['center'], row_positions['center'])
        ])
    elif value == '6':
        positions.extend([
            (col_positions['left'], row_positions['top']),
            (col_positions['left'], row_positions['center']),
            (col_positions['left'], row_positions['bottom']),
            (col_positions['right'], row_positions['top']),
            (col_positions['right'], row_positions['center']),
            (col_positions['right'], row_positions['bottom'])
        ])
    if value == '7':
        # Six icons in two vertical lines, one in the center
        positions.extend([
            (col_positions['left'], row_positions['top']),
            (col_positions['left'], row_positions['center']),
            (col_positions['left'], row_positions['bottom']),
            (col_positions['center'], row_positions['center']),
            (col_positions['right'], row_positions['top']),
            (col_positions['right'], row_positions['center']),
            (col_positions['right'], row_positions['bottom'])
        ])
    elif value == '8':
        # Six icons in two vertical lines, two additional in the vertical center
        positions.extend([
            (col_positions['left'], row_positions['top']),
            (col_positions['left'], row_positions['center']),
            (col_positions['left'], row_positions['bottom']),
            (col_positions['right'], row_positions['top']),
            (col_positions['right'], row_positions['center']),
            (col_positions['right'], row_positions['bottom']),
            (col_positions['center'], row_positions['upper_mid']),
            (col_positions['center'], row_positions['lower_mid'])
        ])
    elif value == '9':
        # Eight icons in two vertical lines, one in the middle top and one middle bottom
        positions.extend([
            (col_positions['left'], row_positions['top']),
            (col_positions['left'], row_positions['upper_mid']),
            (col_positions['left'], row_positions['lower_mid']),
            (col_positions['left'], row_positions['bottom']),
            (col_positions['right'], row_positions['top']),
            (col_positions['right'], row_positions['upper_mid']),
            (col_positions['right'], row_positions['lower_mid']),
            (col_positions['right'], row_positions['bottom']),
            (col_positions['center'], row_positions['center'])
        ])
    elif value == '10':
        # Nine icons in three vertical lines, making it similar to a full grid
        positions.extend([
            (col_positions['left'], row_positions['top']),
            (col_positions['left'], row_positions['upper_mid']),
            (col_positions['left'], row_positions['lower_mid']),
            (col_positions['left'], row_positions['bottom']),
            (col_positions['center'], row_positions['upper_center']),
            (col_positions['center'], row_positions['lower_center']),
            (col_positions['right'], row_positions['top']),
            (col_positions['right'], row_positions['upper_mid']),
            (col_positions['right'], row_positions['lower_mid']),
            (col_positions['right'], row_positions['bottom'])
        ])
    return positions

def generate_joker_card(theme, text_color):
    # Chemins de base pour le fond et les bordures
    background_path = f'components/background.png'
    border_path = f'components/{theme}/border-{theme[0].upper()}.png'
    joker_image_path = f'components/{theme}/Loki-{theme[0].upper()}.png'  # Assurez-vous que ce chemin est correct

    # Chargement et redimensionnement des images de base
    background = Image.open(background_path).convert('RGBA').resize(card_size, Image.LANCZOS)
    border = Image.open(border_path).convert('RGBA').resize(card_size, Image.LANCZOS)
    joker_image = Image.open(joker_image_path).convert('RGBA')
    resized_joker_image = resize_image(joker_image, figure_size)

    # Composition de la carte
    card = Image.alpha_composite(background, border)

    # Positionnement de l'image du Joker
    joker_position = ((card_size[0] - resized_joker_image.width) // 2,
                      (card_size[1] - resized_joker_image.height) // 2)
    card.paste(resized_joker_image, joker_position, resized_joker_image)

    # Ajout de l'inscription "JOKER"
    draw = ImageDraw.Draw(card)
    font = ImageFont.truetype('/home/lexit/font/Norse-Bold.otf', size=80)
    text_position = (card_size[0] // 2 - draw.textsize("JOKER", font=font)[0] // 2,
                     joker_position[1] - 100)  # Ajustez cette valeur selon vos besoins
    draw.text(text_position, "JOKER", font=font, fill=text_color)

    save_path = f'./{theme}-cards/'
    save_name = f'Joker_{text_color}.png'
    # Sauvegarde de la carte
    card.save(f'{save_path}{save_name}')


def generate_card_backs():
    themes = ['dark', 'light', 'lavande', 'orange']
    for theme in themes:
        # Chemin vers l'image de dos de la carte pour le thème courant
        back_image_path = f'components/{theme}/card_back.png'
        save_path = f'./{theme}-cards/back.png'

        # Chargement et redimensionnement de l'image de dos
        back_image = Image.open(back_image_path).convert('RGBA')
        resized_back_image = back_image.resize(card_size, Image.LANCZOS)

        # Enregistrement de l'image de dos dans le dossier correspondant
        resized_back_image.save(save_path)

    print("All card backs have been generated and saved.")


def generate_card(value, suit, theme):
    # Paths and basic setup
    background_path = f'components/background.png'
    border_path = f'components/{theme}/border-{theme[0].upper()}.png'
    decoration_path = 'components/decorations-R.png' if suit in ['H', 'D'] else 'components/decorations-B.png'
    suit_icon_path = f'components/{suit}.png'
    small_suit_icon_path = f'components/{suit}.png'  # Assuming same icon can be resized for small
    text_color = 'black' if suit in ['S', 'C'] else 'red'

    # Opening and resizing basic components
    background = Image.open(background_path).convert('RGBA').resize(card_size, Image.LANCZOS)
    border = Image.open(border_path).convert('RGBA').resize(card_size, Image.LANCZOS)
    decoration = Image.open(decoration_path).convert('RGBA').resize((int(card_size[0] * 0.9), int(card_size[1] * 0.9)), Image.LANCZOS)
    suit_icon = Image.open(suit_icon_path).convert('RGBA')

    # Composite base card
    card = Image.alpha_composite(background, border)
    decoration_position = ((card_size[0] - decoration.width) // 2, (card_size[1] - decoration.height) // 2)
    card.paste(decoration, decoration_position, decoration)

    # Place main icons based on value
    icon_sizes = {'as': as_icon_size, 'big': big_icon_size, 'medium': medium_icon_size}
    place_icons(card, suit_icon, value, card_size, icon_sizes)

    # Drawing values and small suit icons
    draw = ImageDraw.Draw(card)
    font = ImageFont.truetype('/home/lexit/font/Norse-Bold.otf', size=80)
    small_suit_icon = Image.open(small_suit_icon_path).convert('RGBA')
    small_resized_icon = resize_icon(small_suit_icon, small_icon_size)

    # Position adjustments for small suit icons
    small_icon_offset_x = -14  # Adjust as needed to position correctly
    small_icon_offset_y = 90  # Adjust as needed for vertical alignment

    if value == '10':
        new_top_left_value_position = (top_left_value_position[0] - 10, top_left_value_position[1])
        new_bottom_right_value_position = (bottom_right_value_position[0] - 10, bottom_right_value_position[1])
        draw.text(new_top_left_value_position, value, font=font, fill=text_color)
        draw.text(new_bottom_right_value_position, value, font=font, fill=text_color)
        # Placing small suit icons for 10
        card.paste(small_resized_icon, (new_top_left_value_position[0] + small_icon_offset_x + 10, new_top_left_value_position[1] + small_icon_offset_y), small_resized_icon)
        card.paste(small_resized_icon, (new_bottom_right_value_position[0] + small_icon_offset_x + 10, new_bottom_right_value_position[1] + small_icon_offset_y), small_resized_icon)
    else:
        draw.text(top_left_value_position, value, font=font, fill=text_color)
        draw.text(bottom_right_value_position, value, font=font, fill=text_color)
        # Placing small suit icons for other values
        card.paste(small_resized_icon, (top_left_value_position[0] + small_icon_offset_x, top_left_value_position[1] + small_icon_offset_y), small_resized_icon)
        card.paste(small_resized_icon, (bottom_right_value_position[0] + small_icon_offset_x, bottom_right_value_position[1] + small_icon_offset_y), small_resized_icon)
    # Gestion des cartes de figures
    if value in ['J', 'Q', 'K']:
        name = {'J': 'jack', 'Q': 'valkyrie', 'K': 'odin'}[value]
        face_image_path = f'components/{theme}/{name}-{theme[0].upper()}.png'
        face_image = Image.open(face_image_path).convert('RGBA')
        resized_face_image = resize_image(face_image, figure_size)
        face_position = ((card_size[0] - resized_face_image.width) // 2, (card_size[1] - resized_face_image.height) // 2)
        card.paste(resized_face_image, face_position, resized_face_image)

    card_name = f'{value}{suit}.png'
    file_path = f'./{theme}-cards/'
    card.save(file_path + card_name)


if __name__ == '__main__':
    # Boucle pour générer toutes les cartes régulières avec barre de progression
    values = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
    suits = ['H', 'D', 'S', 'C']
    themes = ['dark', 'light', 'lavande', 'orange']

    # Calcul du nombre total de cartes à générer pour initialiser la barre de progression
    total_cards = len(values) * len(suits) * len(themes)

    with tqdm(total=total_cards, desc="Generating regular cards") as pbar:
        for value in values:
            for suit in suits:
                for theme in themes:
                    generate_card(value, suit, theme)
                    pbar.update(1)  # Mise à jour de la barre de progression après chaque carte

    # Génération des cartes Joker avec barre de progression
    with tqdm(total=len(themes) * 2, desc="Generating Joker cards") as pbar:
        for theme in themes:
            generate_joker_card(theme, 'red')
            pbar.update(1)  # Mise à jour après chaque Joker rouge
            generate_joker_card(theme, 'black')
            pbar.update(1)  # Mise à jour après chaque Joker noir

    # Génération des dos de cartes
    generate_card_backs()